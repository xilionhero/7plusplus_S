#ifndef __GPO_h_
#define __GPO_h_
#pragma once
#include "gameplay.h"
//#include "PlayerTank.h"

#include "GameplayObject.h"
#include "src/ObstacleObject.h"
#include "Bullet.h"

class PlayerTank;

using namespace gameplay;

// This class is used to control all the current objects in the scene if they are active
class ObjectController
{
public:
	ObjectController();
	~ObjectController();

	// All Objects in the current scene
	std::vector<GameplayObject*> allObjects = std::vector<GameplayObject*>();
	std::vector<PlayerTank*> allTanks = std::vector<PlayerTank*>();
	std::vector<ObstacleObject*> allObstacles = std::vector<ObstacleObject*>();

	// The Update methode to call on each gameobject
	void Update(float duration);

	// The Draw methode to call on each gameobject
	void Draw(float duration);

	// Add a new item to the list
	void AddToList(GameplayObject* object);

	// Safetely remove a item from the list
	void RemoveFromList(GameplayObject* object);

	// Start a new round.
	void startNewRound(Scene* _Scene);
	void DeleteBullet(Bullet* bullet);
};

#endif