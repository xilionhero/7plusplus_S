#include "CrosshairBuilder.h"
#include "Bundle.h"


CrosshairBuilder::CrosshairBuilder(gameplay::Scene * scene)
{
	pScene = scene;
}

CrosshairBuilder::~CrosshairBuilder()
{
}

void CrosshairBuilder::Initialize()
{
	//Get the model from the .gpb file and specify which shape you want to use (can be found in the Maya project).
	gameplay::Bundle* pBundle = gameplay::Bundle::create("res/crosshair.gpb");
	pOriginalNode = pBundle->loadNode("pPlane1");

	// Set the material and the model.
	pCrosshairModel = dynamic_cast<gameplay::Model*>(pOriginalNode->getDrawable());
	pCrosshairModel->setMaterial(("res/demo.material#crosshair"));
	pCrosshairMaterial = pCrosshairModel->getMaterial();

	SAFE_RELEASE(pBundle);
}

//Remember to call this method in PlayerTank, because the tank will be spawning the crosshair along with the bullet
Crosshair * CrosshairBuilder::BuildCrosshair()
{
	gameplay::Node* pCrosshairNode = pOriginalNode->clone();

	pCrosshairNode->setId("Crosshair");
	pCrosshairNode->scale(1.0f, 1.0f, 1.0f); //Size of the crosshair.
	pCrosshairNode->setTranslationY(0.01f); //Put the crosshair a little above the ground.

	Crosshair* pCrosshair = new Crosshair();
	pCrosshair->myNode = pCrosshairNode;

	pScene->addNode(pCrosshairNode);

	return pCrosshair;
}


