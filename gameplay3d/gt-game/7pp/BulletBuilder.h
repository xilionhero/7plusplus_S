#pragma once
#include "Scene.h"
#include "Bullet.h"
#include "src/Collide_Fine.h"

class BulletBuilder
{
public:

	BulletBuilder(gameplay::Scene* scene);

	~BulletBuilder();
	void Initialize();

	Bullet* BuildBullet();

	gameplay::Scene* pScene;
	gameplay::Node* pOriginalNode;
	gameplay::Model* pBulletModel;
	gameplay::Material* pBulletMaterial;
	cyclone::CollisionBox bulletCollisionBox;
};

