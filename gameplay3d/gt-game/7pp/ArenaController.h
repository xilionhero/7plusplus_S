#pragma once
#include "Scene.h"
#include "ObjectController.h"
#include "BulletBuilder.h"
#include "EffectsManager.h"
#include "CrosshairBuilder.h"

class InputManager;

using namespace gameplay;

class IInputHandler;
class PlayerTank;
//class ObjectController;

/**
* This class controls the Arena
*/
class ArenaController
{
public:
	ArenaController();
	~ArenaController();

	/**
	* This method needs to be called from a player dies and needs the playerID
	*/
	void PlayerDeath(int);

	/**
	* This method needs to be called at the start of the game to set all default values and
	* spawn the right amount of tank
	* It requires: number of players / current scene / current objectcontroller
	*/
	void StartGame(int, Scene*, ObjectController*, InputManager*, EffectsManager* fxManager);

	/*PlayerTank* tank;
	PlayerTank*	tank2;*/

private:
	int amountOfAlivePlayers;
	int totalAmountOfPlayers;
	std::vector<gameplay::Vector3> allSpawnPoints = std::vector<gameplay::Vector3>();
	/**
	* This method is called by PlayerDeath() to see if there is only 1 player left
	* if so, then this method handles the winning situation in the game
	*/
	void CheckWin();

	BulletBuilder* bulletBuilder;
	CrosshairBuilder* crosshairBuilder;

};