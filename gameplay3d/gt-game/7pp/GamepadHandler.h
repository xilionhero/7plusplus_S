#pragma once

#include <map>

#include "IInputHandler.h"
#include "Base.h"
#include "Gamepad.h"

class GamepadHandler : public IInputHandler
{
public:

	friend class InputManager;

	bool IsButtonDown(gameplay::Gamepad::ButtonMapping button);
	bool IsButtonUp(gameplay::Gamepad::ButtonMapping button);
	bool IsButtonDownOnce(gameplay::Gamepad::ButtonMapping button);
	bool IsButtonUpOnce(gameplay::Gamepad::ButtonMapping button);

	gameplay::Vector2 GetDriveDirection();
	float GetDriveDirectionX();
	float GetDriveDirectionY();
	gameplay::Vector2 GetAimDirection();
	float GetAimDirectionX();
	float GetAimDirectionY();
	bool IsPrimaryShooting();
	bool IsPrimaryShootingOnce();
	float GetPrimaryShootingValue();
	bool IsSecondaryShooting();
	bool IsSecondaryShootingOnce();
	bool IsSecondaryShootingReleased();
	float GetSecondaryShootingValue();

	// TODO(Almar): fill in rest of the menu actions
	bool MenuUp();
	bool MenuUpOnce();
	bool MenuRight();
	bool MenuRightOnce();
	bool MenuDown();
	bool MenuDownOnce();
	bool MenuLeft();
	bool MenuLeftOnce();
	bool MenuSelect();
	bool MenuSelectOnce();
	bool MenuBack();
	bool MenuBackOnce();
	bool MenuPause();
	bool MenuPauseOnce();

	bool IsDeviceConnected();

	// 0 to 1
	void SetLeftVibration(float power);
	void SetRightVibration(float power);
	
private:

	void _UpdateButtonState(gameplay::Gamepad::ButtonMapping button);
	void _UpdateTriggerStates();

	gameplay::Gamepad* _pGamepad;
	bool _isConnected = false;

	std::map<gameplay::Gamepad::ButtonMapping, bool> _previousButtonStates;
	std::map<gameplay::Gamepad::ButtonMapping, bool> _buttonStates;

	float _previousLeftTriggerState;
	float _leftTriggerState;

	float _previousRightTriggerState;
	float _rightTriggerState;

};