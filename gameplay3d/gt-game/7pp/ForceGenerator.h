#pragma once
#include "src/RigidBody.h"

class ForceGenerator {

public:

	virtual void updateForce(cyclone::RigidBody *physicsObject, float duration) = 0;

protected:


private:

};

class Gravity : public ForceGenerator
{
	Vector3 gravity;

public:
	// Creates the generator with the given acceleration.
	Gravity(const Vector3 &gravity);

	// Updates the gravitational force on PhysicsObjects.
	virtual void updateForce(cyclone::RigidBody *physicsObject, float duration);

};
