#pragma once
#include "Node.h"

class Shockwave
{
public:
	Shockwave();
	~Shockwave();
	void Update(float elapsedTime);
	void CreateShockwave(gameplay::Scene* scene, gameplay::Node* node, float startT, gameplay::Vector2 direction);

	gameplay::Vector2 dir;
	gameplay::Node* shockNode;
	float startTime;
	bool dead;
	gameplay::Scene* scene;
};

