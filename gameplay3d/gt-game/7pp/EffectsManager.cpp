#include "EffectsManager.h"
#include "Bundle.h"
#include "Shockwave.h"
#include "src/GTGame.h"


EffectsManager::EffectsManager(Scene* activeScene, GTGame* mainScr)
{
	_startTime = mainScr->getTime();
	_pActiveScene = activeScene;
	_pMainScrR = mainScr;

	//// Load the bundles for the different effects.
	//_pFxBundle = Bundle::create("res/Donut.gpb");

	//// Load the shockwave.
	//_pShockWaveTest = _pFxBundle->loadNode("pTorus1");
	//_pShockWaveTest->setId("ShockWave");
	//_pShockWaveTest->setRotation(Vector3(0, 0, 1), 1.64);
	//_pShockWaveTest->setScale(Vector3(0.75, 0.75, 0.75));

	//// Set the material and the model.
	//Model* sWModel = dynamic_cast<Model*>(_pShockWaveTest->getDrawable());
	//// DEBUG: Remember to use demo.material#lambert2 for just shape debug.
	//sWModel->setMaterial("res/shockwave.material#shockwave");
	//Material* sWMat = sWModel->getMaterial();
	//// Bind the framebuffer texture to the shader so it can be sampled.
	//sWMat->getParameter("u_refractionTexture")->setSampler(_pMainScrR->frameBuffTexSam);
	//sWMat->getParameter("u_time")->bindValue(mainScr, &GTGame::getTime);

	//// Add smoke particle emitter to node.
	//_pSmokePartEmit = ParticleEmitter::create("res/particles/smoke.particle");
	//_pSmokePartEmit->setTexture("res/particles/smoke.png", _pSmokePartEmit->getBlendMode());
	//_pSmokePartNode = Node::create("smokePartNode");
	//_pSmokePartNode->setDrawable(_pSmokePartEmit);
	//// Rotation relative to parent.
	//_pSmokePartNode->setRotation(Vector3(0, 0, 1), 3.14);
	//_pSmokePartEmit->start();
	//_pShockWaveTest->addChild(_pSmokePartNode);

	//_pFirePartEmit = ParticleEmitter::create("res/particles/fire.particle");
	//_pFirePartEmit->setTexture("res/particles/fire.png", _pFirePartEmit->getBlendMode());
	//_pFirePartNode = Node::create("firePartNode");
	//_pFirePartNode->setDrawable(_pFirePartEmit);
	//activeScene->addNode(_pFirePartNode);

	//activeScene->addNode(_pShockWaveTest);
}


EffectsManager::~EffectsManager()
{
	for (std::vector<Shockwave*>::iterator i = _pShockwaves.begin(); i != _pShockwaves.end(); ++i)
	{
		delete *i;
	}
	SAFE_RELEASE(_pFxBundle);
}

void EffectsManager::Update(float totalTime)
{
	for (int i = 0; i < _pShockwaves.size(); ++i)
	{
		if (!_pShockwaves[i]->dead)
			_pShockwaves[i]->Update(totalTime);
		else
		{
			//Delete node...
			delete _pShockwaves[i];
			_pShockwaves.erase(_pShockwaves.begin() + i);
		}
	}
}

void EffectsManager::SpawnShockwave(Vector3 spawnPos, Vector2 direction)
{
	// Load the shockwave.
	Bundle* fxBundle = Bundle::create("res/Donut.gpb");
	Node* nShockw = fxBundle->loadNode("pTorus1");
	//Bundle* _pFxBundle = Bundle::create("res/demo.gpb");
	//Node* nShockw = _pFxBundle->loadNode("box");
	nShockw->setId("ShockWave");
	float rotation = (float)atan2(-direction.y, direction.x);
	//nShockw->setRotation(Vector3(0, 0, 1), 1.64);
	nShockw->rotateZ(rotation);
	nShockw->setScale(Vector3(0.6, 0.6, 0.6));
	nShockw->setTranslation(spawnPos);

	// Set the material and the model.
	Model* sWModel = dynamic_cast<Model*>(nShockw->getDrawable());
	// DEBUG: Remember to use demo.material#lambert2 for just shape debug.
	sWModel->setMaterial("res/shockwave.material#shockwave");
	Material* sWMat = sWModel->getMaterial();
	// Bind the framebuffer texture to the shader so it can be sampled.
	sWMat->getParameter("u_refractionTexture")->setSampler(_pMainScrR->frameBuffTexSam);
	sWMat->getParameter("u_time")->bindValue(_pMainScrR, &GTGame::getTime);

	_pActiveScene->addNode(nShockw);

	Shockwave* sw = new Shockwave();
	sw->CreateShockwave(_pActiveScene, nShockw, _pMainScrR->getTime(), direction);
	_pShockwaves.push_back(sw);
}

void EffectsManager::ClearShockwavesList(Scene * _Scene)
{
	for (int i = 0; i < _pShockwaves.size(); i++)
	{
		_Scene->removeNode(_pShockwaves[i]->shockNode);
		_pShockwaves.clear();
	}
}
