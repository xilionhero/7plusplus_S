material colored
{
    u_worldViewProjectionMatrix = WORLD_VIEW_PROJECTION_MATRIX
    
    renderState
    {
        cullFace = true
        depthTest = true
    }
    
    technique
    {
        pass 
        {
            vertexShader = res/shaders/colored.vert
            fragmentShader = res/shaders/colored.frag
        }
    }
}

material lambert2 : colored
{
    u_diffuseColor = 0.2, 0.2, 0.2, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}

material texturedUnlit
{
    technique
    { 
        pass 0
        {
            // shaders
            vertexShader = res/shaders/textured.vert
            fragmentShader = res/shaders/textured.frag
            defines = TEXTURE_REPEAT
           
            // uniforms
            u_worldViewProjectionMatrix = WORLD_VIEW_PROJECTION_MATRIX
            u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX
            u_cameraPosition = CAMERA_WORLD_POSITION
           
            // samplers
            sampler u_diffuseTexture
            {
                
                mipmap = true
                wrapS = REPEAT
                wrapT = REPEAT
                minFilter = LINEAR_MIPMAP_LINEAR
                magFilter = LINEAR
            }
            // render state
            renderState
            {
                cullFace = true
                depthTest = true
            }
        }
    }
}

material texturedTransparent
{
    technique
    {
        pass 0
        {
            vertexShader = res/shaders/textured.vert
            fragmentShader = res/shaders/textured.frag
            defines = DIRECTIONAL_LIGHT_COUNT 1

            u_worldViewProjectionMatrix = WORLD_VIEW_PROJECTION_MATRIX
            u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX
            
            sampler u_diffuseTexture
            {
                mipmap = true
                wrapS = CLAMP
                wrapT = CLAMP
                minFilter = LINEAR_MIPMAP_LINEAR
                magFilter = LINEAR
            }

            renderState
            {
                cullFace = true
                depthTest = true
	            blend = true
                blendSrc = SRC_ALPHA
                blendDst = ONE_MINUS_SRC_ALPHA
            }
        }
    }
}

material texturedSpecular
{
    technique
    {
        pass
        {
            vertexShader = res/shaders/textured.vert
            fragmentShader = res/shaders/textured.frag
			defines = SPECULAR;DIRECTIONAL_LIGHT_COUNT 1

            u_worldViewProjectionMatrix = WORLD_VIEW_PROJECTION_MATRIX
            u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX
            u_worldViewMatrix = WORLD_VIEW_MATRIX
            u_cameraPosition = CAMERA_VIEW_POSITION
            u_ambientColor = SCENE_AMBIENT_COLOR
            u_specularExponent = 50

            sampler u_diffuseTexture
            {
				path = res/png/camo.png
                mipmap = true
                wrapS = CLAMP
                wrapT = CLAMP
                minFilter = LINEAR_MIPMAP_LINEAR
                magFilter = LINEAR
            }

            renderState
            {
                cullFace = true
                depthTest = true
            }
        }
    }
}

material tank : texturedUnlit
{
    technique
    {
        pass 0
        {
		u_textureRepeat = 2, 2

            sampler u_diffuseTexture
            {
                path = res/png/camo.png
            }
        }
    }
}

material tankred : texturedUnlit
{
    technique
    {
        pass 0
        {
		u_textureRepeat = 2, 2

            sampler u_diffuseTexture
            {
                path = res/png/redcamo.png
            }
        }
    }
}

material tankblue : texturedUnlit
{
    technique
    {
        pass 0
        {
		u_textureRepeat = 2, 2

            sampler u_diffuseTexture
            {
                path = res/png/bluecamo.png
            }
        }
    }
}

material tankyellow : texturedUnlit
{
    technique
    {
        pass 0
        {
		u_textureRepeat = 2, 2

            sampler u_diffuseTexture
            {
                path = res/png/yellowcamo.png
            }
        }
    }
}

material ground : texturedUnlit
{
    technique
    {
        pass 0
        {
		u_textureRepeat = 5, 5

            sampler u_diffuseTexture
            {
                path = res/png/dirtwhite.png
            }
        }
    }
}

material wall : texturedUnlit
{
    technique
    {
        pass 0
        {
		u_textureRepeat = 7, 1

            sampler u_diffuseTexture
            {
                path = res/png/brick.png
            }
        }
    }
}

material crosshair : texturedTransparent
{
    technique
    {
        pass 0
        {
            sampler u_diffuseTexture
            {
                path = res/png/Cursor.png
            }
        }
    }
}