#ifdef OPENGL_ES
precision highp float;
#endif

//////////////////////////
// Uniforms
uniform sampler2D u_refractionTexture;
uniform sampler2D u_normalMap;
uniform float u_time;

//////////////////////////
// Varyings
varying vec4 v_vertexRefractionPosition;
varying vec2 v_texCoord;
varying vec3 v_eyePosition;

//////////////////////////

const float fresPow = 0.95f;
const float distortAmount = 0.10;
const float specularAmount = 2.5;
const float textureRepeat = 2.0;
const vec4 tangent = vec4(1.0, 0.0, 0.0, 0.0);
const vec4 viewNormal = vec4(0.0, 1.0, 0.0, 0.0);
const vec4 bitangent = vec4(0.0, 0.0, 1.0, 0.0);
const vec4 shockwaveColour = vec4(1.0, 1.0, 1.0, 0.1);
const vec4 fresnelColour = vec4(0.15, 0.15, 0.15, 1.0);

vec2 fromClipSpace(vec4 position)
{
    return position.xy / position.w / 2.0 + 0.5;
}

void main()
{	
    // Get normal
    vec4 normal = texture2D(u_normalMap, v_texCoord * textureRepeat + u_time * 10);
    normal = normalize(normal * 2.0 - 1.0);
    
    // Distortion offset
    vec4 dudv = normal * distortAmount;

    // Refraction sample
    vec2 textureCoord = fromClipSpace(v_vertexRefractionPosition) + dudv.rg;
    textureCoord = clamp(textureCoord, 0.001, 0.999);
    vec4 refractionColour = texture2D(u_refractionTexture, textureCoord);// * shockwaveColour;

    vec4 viewDir = normalize(vec4(v_eyePosition, 1.0));

	// Fresnel.
	float oneMinusDot = 1.0 - abs(dot(viewDir, normal));
	// Scalar value.
	float fresnel = pow(oneMinusDot, fresPow);

	//gl_FragColor.a = 0.1;

    gl_FragColor = refractionColour;// + mix(refractionColour, fresnelColour, fresnel);
}