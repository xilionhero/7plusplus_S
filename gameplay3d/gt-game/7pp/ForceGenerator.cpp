#include "ForceGenerator.h"

Gravity::Gravity(const Vector3 & gravity): gravity(gravity)
{
	
}

void Gravity::updateForce(MyPhysicsObject *physicsObject, float duration)
{
	// Add gravity.
	physicsObject->addForce(gravity * physicsObject->getMass());
}
