#pragma once
#include "Scene.h"
#include "Bundle.h"
#include "Node.h"
#include "Model.h"
#include "Material.h"
#include "EffectsManager.h"
#include "src/Collide_Fine.h"
#include "InputManager.h"

//#include "PlayerTank.h"
using namespace gameplay;

class PlayerTank;

class TankBuilder
{
public:
	TankBuilder(Scene*, InputManager*, EffectsManager* fxMan);
	~TankBuilder();

	void SetSpawnPoint(gameplay::Vector3 spawnP);
	void SetName(std::string);
	void SetModel(std::string);
	void SetPhysicsValues(float massV, float dampingV);

	PlayerTank* Build();

	void Clear();

	float tankNr;
	std::string nodeName;
	gameplay::Vector3 spawnPoint;
	float mass;
	float damping;
	EffectsManager* fxMan;
	Scene* scene;
	Node* tankNode;
	Node* tTurretNode;
	Node* tBaseNode;
	Model* tankBaseModel;
	Model* tankTurretModel;
	Material* tankMaterial;
	cyclone::CollisionBox tankCollisionBox;
	InputManager* inputManager;

private:
	
};

