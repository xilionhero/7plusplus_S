#pragma once
#include "GameplayObject.h"

namespace gameplay
{
	class Vector2;
}

class Crosshair : public GameplayObject
{
public:
	Crosshair();
	~Crosshair();

	void Update(float);

};
