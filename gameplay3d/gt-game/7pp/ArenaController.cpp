#include "ArenaController.h"
#include "TankBuilder.h"
#include "PlayerTank.h"
#include "EffectsManager.h"
#include "ObjectController.h"

ArenaController::ArenaController()
{
}


ArenaController::~ArenaController()
{
}

void ArenaController::PlayerDeath(int player)
{
	amountOfAlivePlayers--;
	// destroy tank from player id
	CheckWin();
}

void ArenaController::StartGame(int playerNumber, Scene* scene, ObjectController* objectController, InputManager* inputManager, EffectsManager* fxMan)
{
	bulletBuilder = new BulletBuilder(scene);
	bulletBuilder->Initialize();

	crosshairBuilder = new CrosshairBuilder(scene);
	crosshairBuilder->Initialize();
	//crosshairBuilder->BuildCrosshair();

	amountOfAlivePlayers = playerNumber;
	totalAmountOfPlayers = playerNumber;

	allSpawnPoints.insert(allSpawnPoints.end(), gameplay::Vector3(-10, 0, -10));
	allSpawnPoints.insert(allSpawnPoints.end(), gameplay::Vector3(-10, 0, 10));
	allSpawnPoints.insert(allSpawnPoints.end(), gameplay::Vector3(10, 0, 10));
	allSpawnPoints.insert(allSpawnPoints.end(), gameplay::Vector3(10, 0, -10));

	for (int i = 0; i < playerNumber; i++)
	{
		TankBuilder* tBuilder = new TankBuilder(scene, inputManager, fxMan);
		tBuilder->SetName("Tank");

		if (i == 0)
		{
			tBuilder->SetModel("demo.material#tank");
		}
		else if (i == 1)
		{
			tBuilder->SetModel("demo.material#tankred");
		}
		else if (i == 2)
		{
			tBuilder->SetModel("demo.material#tankblue");
		}
		else
		{
			tBuilder->SetModel("demo.material#tankyellow");
		}
		tBuilder->SetPhysicsValues(10, 0.5f);
		PlayerTank* tank = tBuilder->Build();
		tank->SetBulletBuilder(bulletBuilder);
		tank->SetCrosshairBuilder(crosshairBuilder);
		tank->objCont = objectController;
		tank->spawnPoint = allSpawnPoints[i]; // TODO > dynamic spawn points from maya
		tank->Initialize();
		tank->tankNr = i;
		tank->body->setPosition(tank->spawnPoint);
		objectController->AddToList(tank);
		tBuilder->Clear();

		
		
	}
}


void ArenaController::CheckWin()
{
	if (amountOfAlivePlayers > 1)
		return;
	// win state
}