#include "TankBuilder.h"
#include "PlayerTank.h"
#include "src/RigidBody.h"
#include "EffectsManager.h"


TankBuilder::TankBuilder(Scene* sceneP, InputManager* inputManager, EffectsManager* fxManager)
{
	fxMan = fxManager;
	scene = sceneP;
	this->inputManager = inputManager;
	spawnPoint = gameplay::Vector3(0, 0, 0);
}

TankBuilder::~TankBuilder()
{
	Clear();
}

void TankBuilder::SetSpawnPoint(gameplay::Vector3 spawnP)
{
	spawnPoint = spawnP;
}

void TankBuilder::SetName(std::string nodeN)
{
	nodeName = nodeN;
	tankNode = scene->addNode(nodeN.c_str());
	tankNode->getDrawable();
}

void TankBuilder::SetModel(std::string matName)
{
	Bundle* bundle = Bundle::create("res/TomTank.gpb");


	// Load the turret part.
	Node* turretNode = bundle->loadNode("pCube3");
	turretNode->setId("Turret");

	// Set the material and the model.
	tankTurretModel = dynamic_cast<Model*>(turretNode->getDrawable());
	// DEBUG: Remember to use demo.material#lambert2
	tankTurretModel->setMaterial(("res/" + matName).c_str());
	tankMaterial = tankTurretModel->getMaterial();

	// Add the node to the parent tank node.
	tankNode->addChild(turretNode);
	tTurretNode = turretNode;
	// TODO: Maybe not safe release? Wont it remove the entire node from the game?
	SAFE_RELEASE(turretNode);



	// Load the base part.
	Node* baseNode = bundle->loadNode("Base");
	baseNode->setId("Base");

	// Set the material and the model.
	tankBaseModel = dynamic_cast<Model*>(baseNode->getDrawable());
	tankBaseModel->setMaterial(("res/" + matName).c_str());

	tankNode->addChild(baseNode);
	tBaseNode = baseNode;
	// TODO: Maybe not safe release? Wont it remove the entire node from the game?
	SAFE_RELEASE(baseNode);


	SAFE_RELEASE(bundle);
}

void TankBuilder::SetPhysicsValues(float massV, float dampingV)
{
	mass = massV;
	damping = dampingV;
}

PlayerTank* TankBuilder::Build()
{
	return new PlayerTank(*this);
}

void TankBuilder::Clear()
{
	std::string nodeName = "";
	tankNode = NULL;
	tTurretNode = NULL;
	tBaseNode = NULL;
	tankBaseModel = NULL;
	tankTurretModel = NULL;
	tankMaterial = NULL;
}