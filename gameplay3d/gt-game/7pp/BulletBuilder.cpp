#include "BulletBuilder.h"
#include "Bundle.h"


BulletBuilder::BulletBuilder(gameplay::Scene * scene)
{
	pScene = scene;
}

BulletBuilder::~BulletBuilder()
{
}

void BulletBuilder::Initialize()
{
	gameplay::Bundle* pBundle = gameplay::Bundle::create("res/bullet2.gpb");
	pOriginalNode = pBundle->loadNode("pCone1");

	// Set the material and the model.
	pBulletModel = dynamic_cast<gameplay::Model*>(pOriginalNode->getDrawable());
	pBulletModel->setMaterial(("res/demo.material#lambert2"));
	pBulletMaterial = pBulletModel->getMaterial();

	SAFE_RELEASE(pBundle);
}

Bullet * BulletBuilder::BuildBullet()
{
	gameplay::Node* pBulletNode = pOriginalNode->clone();

	pBulletNode->setId("Bullet");
	pBulletNode->scale(0.1f, 0.1f, 0.1f);
	pBulletNode->setTranslationY(1);

	Bullet* pBullet = new Bullet();
	pBullet->myNode = pBulletNode;
	pBullet->SetPhysicsValues(1, 1);

	pScene->addNode(pBulletNode);

	return pBullet;
}


