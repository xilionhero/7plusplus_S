#pragma once
#include "Node.h"
#include "src/RigidBody.h"
#include "TankBuilder.h"
#include "BulletBuilder.h"
#include "CrosshairBuilder.h"
#include "KeyboardMouseHandler.h"
#include "GamepadHandler.h"
#include "EffectsManager.h"
#include "ObjectController.h"
#include "../7pp/src/Collide_Fine.h"

using namespace gameplay;
using namespace cyclone;

class TankBuilder;

class PlayerTank : public GameplayObject
{
public:
	PlayerTank(TankBuilder tankBuilder)
	{
		fxMan = tankBuilder.fxMan;
		tankNr = tankBuilder.tankNr;
		spawnPoint = tankBuilder.spawnPoint;
		nodeName = tankBuilder.nodeName;
		tankNode = tankBuilder.tankNode;
		tTurretNode = tankBuilder.tTurretNode;
		tBaseNode = tankBuilder.tBaseNode;
		tankBaseModel = tankBuilder.tankBaseModel;
		tankTurretModel = tankBuilder.tankTurretModel;
		tankMaterial = tankBuilder.tankMaterial;
		tankCollisionBox = tankBuilder.tankCollisionBox;

		// Create a pointer to the rigidbody for accessing the object from tank.
		body = new RigidBody();

		// Instance of the body that must be given to the base class.
		tankCollisionBox.body = body;

		// Set the half size of the collision box.
		tankCollisionBox.halfSize = cyclone::Vector3(0.8, 0.4, 0.6);

		myNode = tankNode;

		body->myNode = myNode;
		body->setPosition(cyclone::Vector3(-1000, -1000, 0));
		body->setOrientation(1, 0, 0, 0);
		body->setVelocity(0, 0, 0);
		body->setRotation(0, 0, 0);

		aimDir = gameplay::Vector2(1, 0);
		
		// Need to pass the node for rigidbody integration


		body->setMass(tankBuilder.mass);
		
		// Set inertia tensor (Mass for rotation)
		Matrix3 tensor;
		tensor.setBlockInertiaTensor(tankCollisionBox.halfSize, tankBuilder.mass);
		body->setInertiaTensor(tensor);

		body->setDamping(0.99f, 0.8f);
		body->clearAccumulators();
		body->setAcceleration(cyclone::Vector3::GRAVITY);

		body->setCanSleep(false);
		body->setAwake(true);

		tankCollisionBox.body->calculateDerivedData();
		tankCollisionBox.calculateInternals();

		_pKeyboardMouseHandler = tankBuilder.inputManager->GetKeyboardMouseHandler();
		_pGamepadHandler = tankBuilder.inputManager->GetGamepadHandler();
		_pActiveInputHandler = _pGamepadHandler;
	};
	~PlayerTank();

	void Update(float);
	void RotateTurret();

	void Reset();
	void PrimaryShot();
	void SecondaryShot();
	void Initialize();
	bool CanShoot();
	void SetBulletBuilder(BulletBuilder* bulletBuilder);
	void SetCrosshairBuilder(CrosshairBuilder* crosshairBuilder);
	void GetHit(PlayerTank* shooter);
	void IncreaseScore();

	EffectsManager* fxMan;
	Node* firePartNode;
	ParticleEmitter* firePartEmit;
	int tankNr;
	int tankHP = 100;
	int tankScore = 0;
	gameplay::Vector3 spawnPoint;
	std::string nodeName;
	Node* tankNode;
	Node* tTurretNode;
	Node* tBaseNode;
	Model* tankBaseModel;
	Model* tankTurretModel;
	ObjectController* objCont;
	Material* tankMaterial;
	RigidBody* body;
	CollisionBox tankCollisionBox;
	gameplay::Vector2 aimDir;
	Crosshair* _pCrosshair;
	


private:

	float _crosshairDistance = 0.0;
	float _lifeTime = 0;
	BulletBuilder* _pBulletBuilder;
	CrosshairBuilder* _pCrosshairBuilder;
	float _timer;
	float _cooldown;

	KeyboardMouseHandler* _pKeyboardMouseHandler;
	GamepadHandler* _pGamepadHandler;
	IInputHandler* _pActiveInputHandler;
	AudioSource* _hitSound;
	AudioSource* _shootSound;
	AudioSource* _drivingSound;

};

