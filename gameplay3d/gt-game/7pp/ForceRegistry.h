#pragma once
#include <vector>
#include "MyPhysicsObject.h"
#include "ForceGenerator.h"

/* This class makes a list of all the Force generators
* with their PhysicsObject and links them together in a vector list.
*/
class ForceRegistry
{
public:
	ForceRegistry();
	~ForceRegistry();

	Rigidbody *myPhysicsObject;
	ForceGenerator *fg;

	// Registers a force generator to the give physics object.
	void add(Rigidbody *myObject, ForceGenerator *forceGenerator);

	// Updates all the forces with their attached physics objects.
	void updateForces(float duration);

protected:
	// Make a vector list of our ForceRegistries.
	std::vector<ForceRegistry> registrations;


private:

};

