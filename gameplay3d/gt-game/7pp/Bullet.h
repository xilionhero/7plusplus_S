#pragma once
#include "src/RigidBody.h"
#include "src/Collide_Fine.h"

class PlayerTank;

namespace gameplay
{
	class Vector2;

}
class BulletBuilder;

class Bullet : public GameplayObject
{
public:
	Bullet();
	~Bullet();

	float damage;

	void Update(float);
	void FireFrom(gameplay::Vector3 spawnPos, gameplay::Vector3 direction, bool useGravity);

	void SetPhysicsValues(float bMass, float bDamping);
	
	ParticleEmitter* smokePartEmit;
	Node* smokePartNode;
	int tankNr;
	float damping;
	float mass;
	float speed;
	float rotation;
	cyclone::RigidBody* body;
	cyclone::CollisionBox bulletCollisionBox;

private:

	float _lifeTime = 0;

};