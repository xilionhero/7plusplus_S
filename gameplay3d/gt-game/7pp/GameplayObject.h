#ifndef __foo_h_
#define __foo_h_
#include "gameplay.h"
//#include "src/RigidBody.h"

using namespace gameplay;

class GameplayObject
{
public:
	virtual ~GameplayObject() = default;
	Node *myNode;

	bool activeInScene = true;
	virtual void Update(float duration);
	virtual void Draw(float duration);
};

#endif