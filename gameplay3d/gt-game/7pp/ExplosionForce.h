#pragma once
#include "ForceGenerator.h"
#include "src/RigidBody.h"
#include "src/CoreMath.h"

using namespace gameplay;

class Explosion : public ForceGenerator
{
	/**
	* Tracks how long the explosion has been in operation, used
	* for time-sensitive effects.
	*/
	float timePassed;

public:
	// Properties of the explosion, these are public because
	// there are so many and providing a suitable constructor
	// would be cumbersome:

	/**
	* The location of the detonation of the weapon.
	*/
	cyclone::Vector3 detonation;

	// ... Other Explosion code as before ...


	/**
	* The radius up to which objects implode in the first stage
	* of the explosion.
	*/
	float implosionMaxRadius;

	/**
	* The radius within which objects don't feel the implosion
	* force. Objects near to the detonation aren't sucked in by
	* the air implosion.
	*/
	float implosionMinRadius;

	/**
	* The length of time that objects spend imploding before the
	* concussion phase kicks in.
	*/
	float implosionDuration;

	/**
	* The maximal force that the implosion can apply. This should
	* be relatively small to avoid the implosion pulling objects
	* through the detonation point and out the other side before
	* the concussion wave kicks in.
	*/
	float implosionForce;

	/**
	* The speed that the shock wave is traveling, this is related
	* to the thickness below in the relationship:
	*
	* thickness >= speed * minimum frame duration
	*/
	float shockwaveSpeed;

	/**
	* The shock wave applies its force over a range of distances,
	* this controls how thick. Faster waves require larger
	* thicknesses.
	*/
	float shockwaveThickness;

	/**
	* This is the force that is applied at the very centre of the
	* concussion wave on an object that is stationary. Objects
	* that are in front or behind of the wavefront, or that are
	* already moving outwards, get proportionally less
	* force. Objects moving in towards the centre get
	* proportionally more force.
	*/
	float peakConcussionForce;

	/**
	* The length of time that the concussion wave is active.
	* As the wave nears this, the forces it applies reduces.
	*/
	float concussionDuration;

	/**
	* This is the peak force for stationary objects in
	* the centre of the convection chimney. Force calculations
	* for this value are the same as for peakConcussionForce.
	*/
	float peakConvectionForce;

	/**
	* The radius of the chimney cylinder in the xz plane.
	*/
	float chimneyRadius;

	/**
	* The maximum height of the chimney.
	*/
	float chimneyHeight;

	/**
	* The length of time the convection chimney is active. Typically
	* this is the longest effect to be in operation, as the heat
	* from the explosion outlives the shock wave and implosion
	* itself.
	*/
	float convectionDuration;

public:
	/**
	* Creates a new explosion with sensible default values.
	*/
	Explosion();

	/**
	* Calculates and applies the force that the explosion
	* has on the given rigid body.
	*/
	virtual void updateForce(cyclone::RigidBody * body, float duration);
};