#include "InputManager.h"

using namespace gameplay;
using namespace std;

InputManager::InputManager()
{
}

InputManager::~InputManager()
{
	//if (_pKeyboardMouseHandler)
	//{
	//	delete _pKeyboardMouseHandler;
	//	_pKeyboardMouseHandler = NULL;
	//}

	for (unsigned int i = 0; i < _gamepads.size(); i++)
	{
		_gamepads[i] = NULL;
	}
	_gamepads.clear();

	for (unsigned int i = 0; i < _gamepadHandlers.size(); i++)
	{
		delete _gamepadHandlers[i];
		_gamepadHandlers[i] = NULL;
	}
	_gamepadHandlers.clear();
}

void InputManager::UpdateKeyboardData(gameplay::Keyboard::KeyEvent evt, int key)
{
	_pKeyboardMouseHandler.UpdateKeyboardData(evt, key);
}

void InputManager::UpdateMouseData(Mouse::MouseEvent evt, int x, int y, int wheelDelta)
{
	_pKeyboardMouseHandler.UpdateMouseData(evt, x, y, wheelDelta);
}

void InputManager::UpdateGamepadData(Gamepad::GamepadEvent evt, Gamepad* pGamepad)
{
	Gamepad* pFoundGamepad = NULL;

	switch (evt)
	{
	case Gamepad::GamepadEvent::CONNECTED_EVENT:

		for (unsigned int i = 0; i < _gamepads.size(); i++)
		{
			// if we connect a gamepad that was already connected before
			if (_gamepads[i] == pGamepad)
			{
				pFoundGamepad = pGamepad;
				break;
			}
		}

		if (pFoundGamepad != NULL)
		{
			// update its handler, if any
			for (unsigned int i = 0; i < _gamepadHandlers.size(); i++)
			{
				if (_gamepadHandlers[i]->_pGamepad == pFoundGamepad)
				{
					_gamepadHandlers[i]->_isConnected = true;
					break;
				}
			}
		}
		else
		{
			// else add it to connected pads
			_gamepads.push_back(pGamepad);
		}

		break;

	case Gamepad::GamepadEvent::DISCONNECTED_EVENT:
		
		// if a gamepad disconnects, check if any handlers are using it
		for (unsigned int i = 0; i < _gamepadHandlers.size(); i++)
		{
			if (_gamepadHandlers[i]->_pGamepad == pGamepad)
			{
				// if so, update its status
				_gamepadHandlers[i]->_isConnected = false;
				break;
			}
		}

		break;
	}
}

void InputManager::Update(float elapsedTime)
{
	for (unsigned int i = 0; i < _gamepads.size(); i++)
	{
		_gamepads[i]->update(elapsedTime);
	}
}

KeyboardMouseHandler* InputManager::GetKeyboardMouseHandler()
{
	return &_pKeyboardMouseHandler;
}

GamepadHandler* InputManager::GetGamepadHandler()
{
	// no gamepads have been connected yet
	if (_gamepads.size() == 0)
	{
		return nullptr;
	}

	// can only request another handler when there is a slot left
	if (_gamepads.size() <= _gamepadHandlers.size() && _gamepadHandlers.size() < 4)
	{
		return nullptr;
	}

	GamepadHandler* gamepadHandler = new GamepadHandler();
	gamepadHandler->_pGamepad = _gamepads[_gamepadHandlers.size()];
	gamepadHandler->_isConnected = true;

	_gamepadHandlers.push_back(gamepadHandler);

	return gamepadHandler;
}
