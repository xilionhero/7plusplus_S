#include "ObjectController.h"
#include "Bullet.h"
#include "PlayerTank.h"
#include "src/ObstacleObject.h"


ObjectController::ObjectController()
{
}


ObjectController::~ObjectController()
{
}

void ObjectController::Update(float duration)
{
	// in case of std::List
	/*if (allObjects.size == 0)
	return;
	for (std::list<GameplayObject>::iterator it = allObjects.begin(); it != allObjects.end(); it++)
	{
	if (it->activeInScene)
	{
	it->Update(duration);
	}
	}*/


	if (allObjects.size() != 0)
	{
		for (unsigned int i = 0; i < allObjects.size(); i++)
		{
			if (allObjects[i]->myNode->getId() == 0)
			{
				RemoveFromList(allObjects[i]);
				continue;
			}
			//if (allObjects[i]->activeInScene)
			//{
			allObjects[i]->Update(duration);
			//}

			float dist = 0;
			if (dynamic_cast<Bullet*>(allObjects[i]))
			{

				Bullet* bPt = dynamic_cast<Bullet*>(allObjects[i]);

				for (int j = 0; j < allTanks.size(); j++)
				{
					if (allTanks[j]->tankNr != bPt->tankNr)
					{
						dist = (allObjects[i]->myNode->getTranslation() - allTanks[j]->myNode->getTranslation()).length();
						if (dist < 1)
						{
							PlayerTank* shooter;
							for (int f = 0; f < allTanks.size(); f++)
							{
								if (allTanks[f]->tankNr == bPt->tankNr)
								{
									shooter = allTanks[f];
									break;
								}
							}

							allTanks[j]->GetHit(shooter);
							bPt->myNode->release();
							Scene::getScene()->removeNode(bPt->myNode);
							RemoveFromList(bPt);
							break;
						}
						//allTanks[j]->Reset();
					}
				}
			}
		}
	}

	// MOAR HACKY SHIT
	if (allTanks.size() == 0)
		return;
	for (unsigned int i = 0; i < allTanks.size(); i++)
	{
		if (allTanks[i]->myNode->getId() == 0)
		{
			RemoveFromList(allTanks[i]);
			continue;
		}
		if (allTanks[i]->activeInScene)
		{
			allTanks[i]->Update(duration);
		}
	}
}

void ObjectController::Draw(float duration)
{
	if (allObjects.size() != 0)
	{
		for (unsigned int i = 0; i < allObjects.size(); i++)
		{
			if (allObjects[i]->myNode->getId() == 0)
			{
				RemoveFromList(allObjects[i]);
				continue;
			}
			if (allObjects[i]->activeInScene)
			{
				allObjects[i]->Draw(duration);
			}
		}
	}

	// HACKY SHIT
	if (allTanks.size() == 0)
		return;
	for (unsigned int i = 0; i < allTanks.size(); i++)
	{
		if (allTanks[i]->myNode->getId() == 0)
		{
			RemoveFromList(allTanks[i]);
			continue;
		}
		if (allTanks[i]->activeInScene)
		{
			allTanks[i]->Draw(duration);
		}
	}

	if (allObstacles.size() == 0)
		return;
	for (unsigned int i = 0; i < allObstacles.size(); i++)
	{
		if (allObstacles[i]->myNode->getId() == 0)
		{
			RemoveFromList(allObstacles[i]);
			continue;
		}
		if (allObstacles[i]->activeInScene)
		{
			allObstacles[i]->Draw(duration);
		}
	}
}

void ObjectController::AddToList(GameplayObject* object)
{
	if (typeid(*object) == typeid(PlayerTank))
	{
		allTanks.insert(allTanks.begin(), dynamic_cast<PlayerTank*>(object));
	}
	else if (typeid(*object) == typeid(ObstacleObject))
	{
		allObstacles.insert(allObstacles.begin(), dynamic_cast<ObstacleObject*>(object));
	}
	else
		allObjects.insert(allObjects.begin(), object);
}

void ObjectController::RemoveFromList(GameplayObject* object)
{
	for (unsigned int i = 0; i < allObjects.size(); i++)
	{
		if (allObjects[i]->myNode == object->myNode)
		{
			allObjects.erase(allObjects.begin() + i);
		}
	}
	//allObjects.erase(std::remove(allObjects.begin(), allObjects.end(), &object), allObjects.end());
	//int pos = std::find_if(allObjects.begin(), allObjects.end(), object) - allObjects.begin();
	/*if (pos >= allObjects.size())
	return;*/
	//allObjects.erase(allObjects.begin() + pos);
}

void ObjectController::startNewRound(Scene* _Scene)
{

	// Reset all the tank's positions towards their initial spawn location.
	for (int i = 0; i < allTanks.size(); i++)
	{
		allTanks[i]->Reset();
	}

	// Delete all the existing bullets.
	for (int j = allObjects.size() - 1; j < allObjects.size(); j--)
	{
		if (typeid(*allObjects[j]) == typeid(Bullet))
		{
			allObjects[j]->myNode->release();
			_Scene->removeNode(allObjects[j]->myNode);
			RemoveFromList(allObjects[j]);
		}
	}
}

void ObjectController::DeleteBullet(Bullet* bullet)
{
	bullet->myNode->release();
	Scene::getScene()->removeNode(bullet->myNode);
	RemoveFromList(bullet);
}
