#include "GamepadHandler.h"
#include "src/GTGame.h"

using namespace gameplay;

bool GamepadHandler::IsDeviceConnected()
{
	return _isConnected;
}

void GamepadHandler::SetLeftVibration(float power)
{
	if (_pGamepad && _isConnected)
	{
		_pGamepad->setLeftVibration(power);
	}
}

void GamepadHandler::SetRightVibration(float power)
{
	if (_pGamepad && _isConnected)
	{
		_pGamepad->setRightVibration(power);
	}
}

void GamepadHandler::_UpdateButtonState(gameplay::Gamepad::ButtonMapping button)
{
	_previousButtonStates[button] = _buttonStates[button];
	_buttonStates[button] = _pGamepad->isButtonDown(button);
}

void GamepadHandler::_UpdateTriggerStates()
{
	_previousLeftTriggerState = _leftTriggerState;
	_leftTriggerState = _pGamepad->getTriggerValue(0);

	_previousRightTriggerState = _rightTriggerState;
	_rightTriggerState = _pGamepad->getTriggerValue(1);
}

bool GamepadHandler::IsButtonDown(Gamepad::ButtonMapping button)
{
	if (!_isConnected)
	{
		return false;
	}

	_UpdateButtonState(button);

	return _buttonStates[button];
}

bool GamepadHandler::IsButtonUp(Gamepad::ButtonMapping button)
{
	if (!_isConnected)
	{
		return false;
	}

	_UpdateButtonState(button);

	return !_buttonStates[button];
}

bool GamepadHandler::IsButtonDownOnce(Gamepad::ButtonMapping button)
{
	if (!_isConnected)
	{
		return false;
	}

	_UpdateButtonState(button);

	return _buttonStates[button] && !_previousButtonStates[button];
}

bool GamepadHandler::IsButtonUpOnce(Gamepad::ButtonMapping button)
{
	if (!_isConnected)
	{
		return false;
	}

	_UpdateButtonState(button);

	return !_buttonStates[button] && _previousButtonStates[button];
}

Vector2 GamepadHandler::GetDriveDirection()
{
	if (!_isConnected)
	{
		return Vector2();
	}

	Vector2 result;
	_pGamepad->getJoystickValues(0, &result);

	result.y = -result.y;

	return result;
}

float GamepadHandler::GetDriveDirectionX()
{
	if (!_isConnected)
	{
		return 0.0f;
	}

	Vector2 result;
	_pGamepad->getJoystickValues(0, &result);

	return result.x;
}

float GamepadHandler::GetDriveDirectionY()
{
	if (!_isConnected)
	{
		return 0.0f;
	}

	Vector2 result;
	_pGamepad->getJoystickValues(0, &result);

	return -result.y;
}

Vector2 GamepadHandler::GetAimDirection()
{
	if (!_isConnected)
	{
		return Vector2();
	}

	Vector2 result;
	_pGamepad->getJoystickValues(1, &result);

	result.y = -result.y;

	return result;
}

float GamepadHandler::GetAimDirectionX()
{
	if (!_isConnected)
	{
		return 0.0f;
	}

	Vector2 result;
	_pGamepad->getJoystickValues(1, &result);

	return result.x;
}

float GamepadHandler::GetAimDirectionY()
{
	if (!_isConnected)
	{
		return 0.0f;
	}

	Vector2 result;
	_pGamepad->getJoystickValues(1, &result);

	return -result.y;
}

bool GamepadHandler::IsPrimaryShooting()
{
	if (!_isConnected)
	{
		return false;
	}

	_UpdateTriggerStates();

	return _rightTriggerState > 0.0f;
}

bool GamepadHandler::IsPrimaryShootingOnce()
{
	if (!_isConnected)
	{
		return false;
	}

	_UpdateTriggerStates();

	return _rightTriggerState > 0.0f && _previousRightTriggerState < 0.001f;
}

float GamepadHandler::GetPrimaryShootingValue()
{
	if (!_isConnected)
	{
		return 0.0f;
	}

	_UpdateTriggerStates();

	return _rightTriggerState;
}

bool GamepadHandler::IsSecondaryShooting()
{
	if (!_isConnected)
	{
		return false;
	}

	_UpdateTriggerStates();

	return _leftTriggerState > 0.0f;
}

bool GamepadHandler::IsSecondaryShootingOnce()
{
	if (!_isConnected)
	{
		return false;
	}

	_UpdateTriggerStates();

	return _leftTriggerState > 0.0f && _previousLeftTriggerState < 0.001f;
}

bool GamepadHandler::IsSecondaryShootingReleased()
{
	if (!_isConnected)
	{
		return false;
	}

	_UpdateTriggerStates();

	return _leftTriggerState < 0.001f;
}

float GamepadHandler::GetSecondaryShootingValue()
{
	if (!_isConnected)
	{
		return 0.0f;
	}

	_UpdateTriggerStates();

	return _leftTriggerState;
}

bool GamepadHandler::MenuUp()
{
	return false;
}

bool GamepadHandler::MenuUpOnce()
{
	return false;
}

bool GamepadHandler::MenuRight()
{
	return false;
}

bool GamepadHandler::MenuRightOnce()
{
	return false;
}

bool GamepadHandler::MenuDown()
{
	return false;
}

bool GamepadHandler::MenuDownOnce()
{
	return false;
}

bool GamepadHandler::MenuLeft()
{
	return false;
}

bool GamepadHandler::MenuLeftOnce()
{
	return false;
}

bool GamepadHandler::MenuSelect()
{
	return false;
}

bool GamepadHandler::MenuSelectOnce()
{
	return false;
}

bool GamepadHandler::MenuBack()
{
	return false;
}

bool GamepadHandler::MenuBackOnce()
{
	return false;
}

bool GamepadHandler::MenuPause()
{
	return false;
}

bool GamepadHandler::MenuPauseOnce()
{
	return false;
}
