#pragma once

#include <vector>

#include "KeyboardMouseHandler.h"
#include "GamepadHandler.h"

class InputManager
{
public:

	friend class GamepadHandler;

	InputManager();
	~InputManager();

	void UpdateKeyboardData(gameplay::Keyboard::KeyEvent evt, int key);
	void UpdateMouseData(gameplay::Mouse::MouseEvent evt, int x, int y, int wheelDelta);
	void UpdateGamepadData(gameplay::Gamepad::GamepadEvent evt, gameplay::Gamepad* pGamepad);

	void Update(float elapsedTime);
	
	KeyboardMouseHandler* GetKeyboardMouseHandler();
	GamepadHandler* GetGamepadHandler();

private:

	KeyboardMouseHandler _pKeyboardMouseHandler;
	std::vector<GamepadHandler*> _gamepadHandlers;
	std::vector<gameplay::Gamepad*> _gamepads;

};