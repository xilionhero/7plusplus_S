#pragma once

#include "Vector2.h"

class IInputHandler
{
public:
	
	/**
	* Represents the input direction for driving the tank.
	* NOTE: negative X means left, positive X means right.
	* NOTE: negative Y means up, positive Y means down.
	*
	* @return A Vector2 containing the X and Y input directions ranging from -1.0F to +1.0F.
	*/
	virtual gameplay::Vector2 GetDriveDirection() = 0;

	/**
	* Represents the horizontal tank driving input direction.
	* NOTE: negative X means left, positive X means right.
	*
	* @return A float ranging from -1.0F to +1.0F.
	*/
	virtual float GetDriveDirectionX() = 0;

	/**
	* Represents the vertical tank driving input direction.
	* NOTE: negative Y means up, positive Y means down.
	*
	* @return A float ranging from -1.0F to +1.0F.
	*/
	virtual float GetDriveDirectionY() = 0;

	/**
	* Represents the input direction for aiming the tank.
	* NOTE: negative X means left, positive X means right.
	* NOTE: negative Y means up, positive Y means down.
	*
	* @return A Vector2 containing the X and Y input directions ranging from -1.0F to +1.0F.
	*/
	virtual gameplay::Vector2 GetAimDirection() = 0;

	/**
	* Represents the horizontal tank aiming input direction.
	* NOTE: negative X means left, positive X means right.
	*
	* @return A float ranging from -1.0F to +1.0F.
	*/
	virtual float GetAimDirectionX() = 0;

	/**
	* Represents the vertical tank aiming input direction.
	* NOTE: negative Y means up, positive Y means down.
	*
	* @return A float ranging from -1.0F to +1.0F.
	*/
	virtual float GetAimDirectionY() = 0;

	/**
	* Represents the action of shooting the primary cannon.
	*
	* @return True as long as the player is firing the primary cannon.
	*/
	virtual bool IsPrimaryShooting() = 0;

	/**
	* Represents the action of shooting the primary cannon once.
	*
	* @return True for a single frame as soon as the player fires the primary cannon.
	*/
	virtual bool IsPrimaryShootingOnce() = 0;

	/**
	* Represents the value of how 'far' or 'hard' the user is firing the primary cannon.
	* NOTE: 0.0F equals 0% and +1.0F equals 100%.
	*
	* @return A float ranging from 0.0F to +1.0F.
	*/
	virtual float GetPrimaryShootingValue() = 0;

	/**
	* Represents the action of shooting the secondary (lob-shot) cannon.
	*
	* @return True as long as the player is firing the secondary cannon.
	*/
	virtual bool IsSecondaryShooting() = 0;

	/**
	* Represents the action of shooting the secondary (lob-shot) cannon once.
	*
	* @return True for a single frame as soon as the player fires the secondary cannon.
	*/
	virtual bool IsSecondaryShootingOnce() = 0;

	virtual bool IsSecondaryShootingReleased() = 0;

	/**
	* Represents the value of how 'far' or 'hard' the user is firing the secondary (lob-shot) cannon.
	* NOTE: 0.0F equals 0% and +1.0F equals 100%.
	*
	* @return A float ranging from 0.0F to +1.0F.
	*/
	virtual float GetSecondaryShootingValue() = 0;

	//virtual bool dash() = 0;
	//virtual bool dashOnce() = 0;

	//virtual bool jump() = 0;
	//virtual bool jumpOnce() = 0;

	//virtual bool reflect() = 0;
	//virtual bool reflectOnce() = 0;

	/**
	* Represents the action of going up in a menu.
	*
	* @return True as long as the player is going up.
	*/
	virtual bool MenuUp() = 0;

	/**
	* Represents the action of going up in a menu once.
	*
	* @return True for a single frame as soon as the player goes up in a menu.
	*/
	virtual bool MenuUpOnce() = 0;

	/**
	* Represents the action of going right in a menu.
	*
	* @return True as long as the player is going right.
	*/
	virtual bool MenuRight() = 0;

	/**
	* Represents the action of going right in a menu once.
	*
	* @return True for a single frame as soon as the player goes right in a menu.
	*/
	virtual bool MenuRightOnce() = 0;

	/**
	* Represents the action of going down in a menu.
	*
	* @return True as long as the player is going down.
	*/
	virtual bool MenuDown() = 0;

	/**
	* Represents the action of going down in a menu once.
	*
	* @return True for a single frame as soon as the player goes down in a menu.
	*/
	virtual bool MenuDownOnce() = 0;

	/**
	* Represents the action of going left in a menu.
	*
	* @return True as long as the player is going left.
	*/
	virtual bool MenuLeft() = 0;

	/**
	* Represents the action of going left in a menu once.
	*
	* @return True for a single frame as soon as the player goes left in a menu.
	*/
	virtual bool MenuLeftOnce() = 0;

	/**
	* Represents the action of selecting an option in a menu.
	*
	* @return True as long as the player is holds down the select button.
	*/
	virtual bool MenuSelect() = 0;

	/**
	* Represents the action of selecting an option in a menu once.
	*
	* @return True for a single frame as soon as the player selects an option.
	*/
	virtual bool MenuSelectOnce() = 0;

	/**
	* Represents the action of going back a step in a menu.
	*
	* @return True as long as the player is holds down the back button.
	*/
	virtual bool MenuBack() = 0;

	/**
	* Represents the action of going back a step in a menu once.
	*
	* @return True for a single frame as soon as the player goes back a step.
	*/
	virtual bool MenuBackOnce() = 0;

	/**
	* Represents the action of pausing the game.
	*
	* @return True as long as the player is holds down the pause button.
	*/
	virtual bool MenuPause() = 0;

	/**
	* Represents the action of pausing the game once.
	*
	* @return True for a single frame as soon as the player pauses te game.
	*/
	virtual bool MenuPauseOnce() = 0;

};