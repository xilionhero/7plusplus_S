#include "ObstacleObject.h"

// Create an Obstacle Object with 
ObstacleObject::ObstacleObject(Scene *_pScene, const char *_pNodeName, float _boxSizeX, float _boxSizeY, float _boxSizeZ)
{
	// Creates an object by searching for the node name in the scene.
	Node * pObstacleNode = _pScene->findNode(_pNodeName);
	pObstacleModel = dynamic_cast<Model*>(pObstacleNode->getDrawable());
	pObstacleMaterial = pObstacleModel->getMaterial();
	myNode = pObstacleNode;

	// Attach a rigidbody to the gameobject.
	body = new cyclone::RigidBody();
	body->myNode = myNode;

	// Give rigidbody to the collision box.
	collisionBox.body = body;
	collisionBox.halfSize = cyclone::Vector3(_boxSizeX, _boxSizeY, _boxSizeZ);
	body->setPosition(Vector3(myNode->getTranslationX(), myNode->getTranslationY(), myNode->getTranslationZ()));

	body->inverseMass = 0;

	body->clearAccumulators();
	//body->setAcceleration(cyclone::Vector3::GRAVITY);

	body->setCanSleep(false);
	body->setAwake(true);

	body->calculateDerivedData();
	collisionBox.calculateInternals();
}


ObstacleObject::~ObstacleObject()
{
}

void ObstacleObject::Update(float elapsedTime) 
{
	body->integrate(elapsedTime);
	collisionBox.calculateInternals();
}