//#ifndef GTGame_H_
//#define GTGame_H_

#pragma once

#include "gameplay.h"
#include "../ObjectController.h"
#include "Collide_Fine.h"
#include "Contacts.h"
#include "../InputManager.h"
#include "../EffectsManager.h"
#include "ObstacleObject.h"

class TankBuilder;
class PlayerTank;
class ArenaController;
//class ObjectController;
class Bullet;


using namespace gameplay;

/**
 * Main game class.
 */
class GTGame: public Game
{
public:
    /**
     * Constructor.
     */
    GTGame();

    /**
     * @see Game::keyEvent
     */
	void keyEvent(Keyboard::KeyEvent evt, int key);

	void newRound();
	void reset();

	bool mouseEvent(Mouse::MouseEvent evt, int x, int y, int wheelDelta);
	void gamepadEvent(Gamepad::GamepadEvent evt, Gamepad* gamepad);

	/**
	* Debug to the visual studio output window using printf syntax.
	*/
	static void Debugf(const char* szFormat, ...);

	void drawCollisionDebug();

	bool hit;
	bool ball_active;
	int tankWinner = 0;
	unsigned fracture_contact;

	FrameBuffer* _frameBuffer;
	Texture::Sampler* frameBuffTexSam;

	/*
	* Return time since game started.
	*/
	float getTime() const
	{
		return Game::getGameTime() * 0.0001;
	}

protected:

    /**
     * @see Game::initialize
     */
    void initialize();

    /**
     * @see Game::finalize
     */
    void finalize();

    /**
     * @see Game::update
     */
    void update(float elapsedTime);

    /**
     * @see Game::render
     */
    void render(float elapsedTime);

	/** Holds the maximum number of contacts. */
	const static unsigned maxContacts = 256;

	/** Holds the array of contacts. */
	cyclone::Contact contacts[maxContacts];

	/** Holds the collision data structure for collision detection. */
	cyclone::CollisionData cData;

	/** Processes the contact generation code. */
	void generateContacts();

	/** Holds the projectile. */
	cyclone::CollisionSphere ball;

private:

    /**
     * Draws the scene each frame.
     */
    bool drawScene(Node* node, bool incRefract);
	bool initializeScene(Node* node);
	void initializeMaterial(Scene* scene, Node* node, Material* material);
	void initializeLevel();
	bool _playedTune;


    Scene* _scene;
	TankBuilder* tBuilder;
	PlayerTank* tank;
	Bullet* bullet;
    bool _wireframe;
	ArenaController* aController;
	ObjectController* objectController;
	InputManager* inputManager;
	EffectsManager* _fxManager;
	/** Holds the contact resolver. */
	cyclone::ContactResolver resolver;
	Font* newFont;
	std::vector<gameplay::Vector2> allUILocations = std::vector<gameplay::Vector2>();
	AudioSource* _backgroundMusic;
	AudioSource* _victorTune;
};

//#endif
