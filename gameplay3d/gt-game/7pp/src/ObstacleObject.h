#pragma once
#include "Node.h"
#include "RigidBody.h"
#include "Collide_Fine.h"
#include <iostream>

using namespace gameplay;

class ObstacleObject : public GameplayObject
{
public:
	ObstacleObject(Scene*, const char*, float, float, float);
	~ObstacleObject();

	void Update(float);

	Model* pObstacleModel;
	Material* pObstacleMaterial;
	cyclone::RigidBody* body;
	cyclone::CollisionBox collisionBox;
};