#include "GTGame.h"
#include "../TankBuilder.h"
#include "../PlayerTank.h"
#include "../ArenaController.h"
#include "../ObjectController.h"
#include "Collide_Fine.h"
#include "ObstacleObject.h"
#include "Contacts.h"
#include "../Bullet.h"
#include <windows.h>
//#include "../ObjectController.h"

#define MAX_BLOCKS 2

static const unsigned int BUFFER_SIZE = 512;
KeyboardMouseHandler* k;

// Declare our game instance
GTGame game;
GTGame::GTGame()
	: _scene(NULL), _wireframe(false), resolver(maxContacts * 8), inputManager(new InputManager())
{
}

void GTGame::initialize()
{
	// Load game scene from file
	_scene = Scene::load("res/ArenaV3.scene");

	// Setup the framebuffer, used for postprocessing purposes.
	_frameBuffer = FrameBuffer::create("refractBuffer", BUFFER_SIZE, BUFFER_SIZE);
	DepthStencilTarget* refractDepthTarget = DepthStencilTarget::create("refractDepth", DepthStencilTarget::DEPTH, BUFFER_SIZE, BUFFER_SIZE);
	_frameBuffer->setDepthStencilTarget(refractDepthTarget);
	SAFE_RELEASE(refractDepthTarget);

	// Texture sampler used for shader framebuffer texture sampling.
	frameBuffTexSam = Texture::Sampler::create(_frameBuffer->getRenderTarget()->getTexture());

	objectController = new ObjectController();
	aController = new ArenaController();
	_fxManager = new EffectsManager(_scene, this);
	aController->StartGame(4, _scene, objectController, inputManager, _fxManager);
	cData.contactArray = contacts;


	_victorTune = AudioSource::create("res/common/horn.ogg", true);
	_backgroundMusic = AudioSource::create("res/common/war.wav", true);
	_backgroundMusic->setGain(0.5f);
	_backgroundMusic->play();

	// Set the aspect ratio for the scene's camera to match the current resolution
	_scene->getActiveCamera()->setAspectRatio(getAspectRatio());

	// Initialize the scene
	_scene->visit(this, &GTGame::initializeScene);

	k = inputManager->GetKeyboardMouseHandler();

	newFont = Font::create("res/ui/arial.gpb");
	allUILocations.insert(allUILocations.end(), gameplay::Vector2(20, 20));
	allUILocations.insert(allUILocations.end(), gameplay::Vector2(20, 140));
	allUILocations.insert(allUILocations.end(), gameplay::Vector2(20, 220));
	allUILocations.insert(allUILocations.end(), gameplay::Vector2(20, 300));
	/*newForm = Form::create("res/editor.form");
	newLabel = Label::create("UI");
	newLabel->setText("Player hp here");
	newLabel->ALIGN_HCENTER;
	newLabel->setFontSize(12);
	newLabel->setTextColor(Vector4(255, 255, 255, 255));
	newLabel->AUTO_SIZE_BOTH;*/
	//newLabel->
	//newLabel->
	//Form::
	initializeLevel();

}

bool GTGame::initializeScene(Node* node)
{
	static Node* lightNode = _scene->findNode("directionalLight1");
	static Node* lightNode2 = _scene->findNode("directionalLight2");

	// Create a node and light attaching the light to the node
	Light* light = Light::createDirectional(gameplay::Vector3(1, .2, 0));
	lightNode->setLight(light);


	Model* model = dynamic_cast<Model*>(node->getDrawable());
	if (model)
	{
		Material* material = model->getMaterial();

		if (material && material->getTechnique()->getPassByIndex(0)->getEffect()->getUniform("u_directionalLightDirection"))
		{
			material->getParameter("u_ambientColor")->setValue(_scene->getAmbientColor());
			material->getParameter("u_directionalLightColor[0]")->setValue(lightNode->getLight()->getColor());
			material->getParameter("u_directionalLightDirection[0]")->setValue(lightNode->getForwardVectorView());
			MaterialParameter* parameter = material->getParameter("u_lightDirection");
			parameter->bindValue(lightNode, &Node::getForwardVectorView);
		}
	}

	return true;
}

void GTGame::initializeMaterial(Scene* scene, Node* node, Material* material)
{
	// Bind light shader parameters to dynamic objects only
	if (node->hasTag("dynamic"))
	{
		material->getParameter("u_ambientColor")->bindValue(scene, &Scene::getAmbientColor);
		Node* lightNode = scene->findNode("directionalLight1");
		Node* lightNode2 = scene->findNode("directionalLight2");
		if (lightNode)
		{
			material->getParameter("u_directionalLightColor[0]")->bindValue(lightNode2->getLight(), &Light::getColor);
			material->getParameter("u_directionalLightDirection[0]")->bindValue(lightNode2, &Node::getForwardVectorView);
		}
	}
}

void GTGame::initializeLevel()
{
	std::map<char const*, gameplay::Vector3> _wallList = {
	{"pCube15", gameplay::Vector3(0.6, 4, 15)},		// Left wall
	{"pCube14", gameplay::Vector3(0.6, 4, 15)},		// Right wall
	{"pCube13", gameplay::Vector3(15, 4, 0.6)},		// Upper wall
	{"pCube12", gameplay::Vector3(15, 4, 0.6)},		// Lower wall		 
	{"pCube5", gameplay::Vector3(0.5, 1, 3.3)},		// Plus wall
	{"pCube6", gameplay::Vector3(3.4, 1, .5)},		// Plus wall
	{"pCube4", gameplay::Vector3(2.3, 1, 0.6)},		// Left inner upper horizontal wall
	{"pCube7", gameplay::Vector3(0.6, 1, 1.6)},		// Left inner middle vertical wall
	{"pCube1", gameplay::Vector3(2.3, 1, 0.6)},		// Left inner lower horizontal wall
	{"pCube10", gameplay::Vector3(2.8, 1, 0.6)},	// Middle inner upper horizontal wall
	{"pCube11", gameplay::Vector3(2.8, 1, 0.6)},	// Middle inner lower horizontal wall
	{"pCube3", gameplay::Vector3(2.3, 1, 0.6)},		// Right inner upper horizontal wall
	{"pCube8", gameplay::Vector3(0.6, 1, 1.6)},		// Right inner middle vertical wall
	{"pCube2", gameplay::Vector3(2.3, 1, 0.6)}		// Right inner lower horizontal wall
	};

	// Iterator to create all walls in the _wallList.
	for (const auto& wallIterator : _wallList)
	{
		ObstacleObject* wall = new ObstacleObject(_scene, wallIterator.first, wallIterator.second.x, wallIterator.second.y, wallIterator.second.z);
		objectController->AddToList(wall);
	}

}

void GTGame::finalize()
{
	SAFE_RELEASE(_scene);
}

void GTGame::update(float elapsedTime)
{
	// To update physics, we need to do this with frametime.
	float t = (float)(elapsedTime / 1000.0f);

	// Update effects.
	_fxManager->Update(getTime());

	inputManager->Update(elapsedTime);

	objectController->Update(t);
	objectController->Draw(t);
	// Rotate model
	//_scene->findNode("pCube2")->rotateY(MATH_DEG_TO_RAD((float)elapsedTime / 1000.0f * 180.0f));
	//_scene->findNode("pCube2")->setTranslation(gameplay::Vector3(200, 0, 0));

	generateContacts();
	resolver.resolveContacts(cData.contactArray, cData.contactCount, t);
	//tank->integrate(t);
	/*aController->tank->integrate(t);
	aController->tank2->integrate(t);*/

}

void GTGame::render(float elapsedTime)
{
	// Update the refract buffer
	FrameBuffer* defaultBuffer = _frameBuffer->bind();
	gameplay::Rectangle defaultViewport = getViewport();
	setViewport(gameplay::Rectangle(BUFFER_SIZE, BUFFER_SIZE));
	clear(CLEAR_COLOR_DEPTH, Vector4::zero(), 1.0f, 0);
	_scene->visit(this, &GTGame::drawScene, false);

	//newForm->draw();

	newFont->start();

	// Draw the final scene
	defaultBuffer->bind();
	setViewport(defaultViewport);
	clear(CLEAR_COLOR_DEPTH, Vector4::zero(), 1.0f, 0);
	_scene->visit(this, &GTGame::drawScene, true);
	//	
	float textSize = newFont->getSize() * 3;
	Vector4 textColour = Vector4(1, 0, 0, 1);

	for (int i = 0; i < 4; i++)
	{
		if (objectController->allTanks.size() > i)
		{
			char integer_string[32];
			sprintf(integer_string, "%d", i + 1);
			char newText[64] = "Player ";
			strcat(newText, integer_string);

			strcat(newText, "\nHP : ");
			sprintf(integer_string, "%d", objectController->allTanks[i]->tankHP);
			strcat(newText, integer_string);

			strcat(newText, "\nScore : ");
			sprintf(integer_string, "%d", objectController->allTanks[i]->tankScore);
			strcat(newText, integer_string);

			newFont->drawText(newText, allUILocations[i].x, allUILocations[i].y, textColour, textSize);

			if (objectController->allTanks[i]->tankScore == 5)
			{
				char integer_string[32];
				sprintf(integer_string, "%d", i + 1);
				char newText[64] = "Player ";
				strcat(newText, integer_string);

				strcat(newText, " Wins!");
				newFont->drawText(newText, Font::ALIGN_HCENTER, Font::ALIGN_VCENTER, textColour, textSize);

				if (!_playedTune)
				{
					_playedTune = true;
					_victorTune->play();
				}

				reset();
			}
		}
	}
	newFont->finish();
}

bool GTGame::drawScene(Node* node, bool incRefract)
{
	std::string id = node->getId();
	if (!incRefract && id == "ShockWave") return true;

	// If the node visited contains a drawable object, draw it
	Drawable* drawable = node->getDrawable();
	if (drawable)
		drawable->draw(_wireframe);

	/*
	 * Use this method to debug objects with collision primitives
	 */
	 //drawCollisionDebug();
	return true;
}

void GTGame::keyEvent(Keyboard::KeyEvent evt, int key)
{
	inputManager->UpdateKeyboardData(evt, key);

	if (evt == Keyboard::KEY_PRESS)
	{
		switch (key)
		{
		case Keyboard::KEY_ESCAPE:
			exit();
			break;

		case Keyboard::KEY_R:
			newRound();
			break;
		}
	}
}

void GTGame::newRound()
{
	// tell managers to clear their nodes in their lists to start "clean".
	_fxManager->ClearShockwavesList(_scene);
	objectController->startNewRound(_scene);
}

void GTGame::reset()
{
	// Call this method to reset the WHOLE scene.
	initialize();
}

bool GTGame::mouseEvent(Mouse::MouseEvent evt, int x, int y, int wheelDelta)
{
	inputManager->UpdateMouseData(evt, x, y, wheelDelta);

	return false;
}

void GTGame::gamepadEvent(Gamepad::GamepadEvent evt, Gamepad* gamepad)
{
	inputManager->UpdateGamepadData(evt, gamepad);
}

void GTGame::generateContacts()
{
	hit = false;

	// Create the ground plane data
	cyclone::CollisionPlane plane;
	plane.direction = cyclone::Vector3(0, 1, 0);
	plane.offset = 0;

	// Set up the collision data structure
	cData.reset(maxContacts);
	cData.friction = (cyclone::real)0.9;
	cData.restitution = (cyclone::real)0.2;
	cData.tolerance = (cyclone::real)0.1;

	// Perform collision detection
	cyclone::Matrix4 transform, otherTransform;
	cyclone::Vector3 position, otherPosition;

	// Tank with tank collision
	for (std::vector<PlayerTank*>::iterator tank = objectController->allTanks.begin(); tank != objectController->allTanks.end(); tank++)
	{
		std::cout << (*tank) << std::endl;
		PlayerTank *block = *tank;
		// Check for collisions with the ground plane
		if (!cData.hasMoreContacts()) return;
		cyclone::CollisionDetector::boxAndHalfSpace(block->tankCollisionBox, plane, &cData);

		if (ball_active)
		{
			// And with the sphere
			if (!cData.hasMoreContacts()) return;
			//			if (cyclone::CollisionDetector::boxAndSphere(block->tankCollisionBox, ball, &cData))
			{
				hit = true;
				fracture_contact = cData.contactCount - 1;
			}
		}

		// Check for collisions with each other box
		std::vector<PlayerTank*>::iterator otherTank = tank;
		for (++otherTank /* Increases otherTank to 2*/; otherTank != objectController->allTanks.end(); otherTank++)
		{
			PlayerTank *other = *otherTank;
			if (!cData.hasMoreContacts()) return;
			cyclone::CollisionDetector::boxAndBox(block->tankCollisionBox, other->tankCollisionBox, &cData);
		}
	}

	// Tank with obstacle 
	for (std::vector<PlayerTank*>::iterator tank = objectController->allTanks.begin(); tank != objectController->allTanks.end(); tank++)
	{
		PlayerTank *block = *tank;
		// Check for collisions with the ground plane
		if (!cData.hasMoreContacts()) return;
		cyclone::CollisionDetector::boxAndHalfSpace(block->tankCollisionBox, plane, &cData);

		if (ball_active)
		{
			// And with the sphere
			if (!cData.hasMoreContacts()) return;
			//			if (cyclone::CollisionDetector::boxAndSphere(block->tankCollisionBox, ball, &cData))
			{
				hit = true;
				fracture_contact = cData.contactCount - 1;
			}
		}

		// Check for collisions with each other box
		for (std::vector<ObstacleObject*>::iterator obstacle = objectController->allObstacles.begin(); obstacle != objectController->allObstacles.end(); obstacle++)
		{
			ObstacleObject *other = *obstacle;
			if (!cData.hasMoreContacts()) return;
			cyclone::CollisionDetector::boxAndBox(block->tankCollisionBox, other->collisionBox, &cData);
		}

	}

	// Obstacle with bullet
	for (std::vector<GameplayObject*>::iterator bullet = objectController->allObjects.begin(); bullet != objectController->allObjects.end(); bullet++)
	{
		if (dynamic_cast<Bullet*>(*bullet))
		{
			Bullet *block = dynamic_cast<Bullet*>(*bullet);

			// Check for collisions with each other box
			for (std::vector<ObstacleObject*>::iterator obstacle = objectController->allObstacles.begin(); obstacle != objectController->allObstacles.end(); obstacle++)
			{
				ObstacleObject *other = *obstacle;
				if (!cData.hasMoreContacts()) return;
				if (cyclone::CollisionDetector::boxAndBox(block->bulletCollisionBox, other->collisionBox, &cData))
				{
					objectController->DeleteBullet(dynamic_cast<Bullet*>(*bullet));
					return;
				}
			}
		}
	}
}

void GTGame::Debugf(const char* szFormat, ...)
{
	char szBuff[1024];
	va_list arg;

	va_start(arg, szFormat);
	_vsnprintf(szBuff, sizeof(szBuff), szFormat, arg);
	va_end(arg);

	OutputDebugStringA(szBuff);
}

void GTGame::drawCollisionDebug()
{

	// Recalculate the contacts, so they are current (in case we're
	// paused, for example).
	generateContacts();

	// Render the contacts, if required
	glBegin(GL_LINES);
	for (unsigned i = 0; i < cData.contactCount; i++)
	{
		// Interbody contacts are in green, floor contacts are red.
		if (contacts[i].body[1]) {
			glColor3f(0, 1, 0);
		}
		else {
			glColor3f(1, 0, 0);
		}

		cyclone::Vector3 vec = contacts[i].contactPoint;
		glVertex3f(vec.x, vec.y, vec.z);

		vec += contacts[i].contactNormal;
		glVertex3f(vec.x, vec.y, vec.z);
	}

	glEnd();
}