#include "Shockwave.h"
#include "Scene.h"



Shockwave::Shockwave()
{
}


Shockwave::~Shockwave()
{
	scene->removeNode(shockNode);
}

void Shockwave::Update(float totalTime) 
{
	if (totalTime - startTime < 0.1)
	{
		// Move shockwave.
		if (shockNode) {
			shockNode->translate(gameplay::Vector3(dir.x, 0, dir.y) * 0.11);
		}
	}
	else if (!dead) 
		dead = true;
}

void Shockwave::CreateShockwave(gameplay::Scene* mScene, gameplay::Node* node, float startT, gameplay::Vector2 direction)
{
	dir = direction;
	startTime = startT;
	scene = mScene;
	shockNode = node;
	dead = false;
}
