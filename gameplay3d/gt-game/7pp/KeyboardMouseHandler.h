#pragma once

#include "IInputHandler.h"

#include <map>

#include "Keyboard.h"
#include "Mouse.h"

class KeyboardMouseHandler : public IInputHandler
{
public:

	bool IsKeyDown(gameplay::Keyboard::Key keyCode);
	bool IsKeyUp(gameplay::Keyboard::Key keyCode);
	bool IsKeyDownOnce(gameplay::Keyboard::Key keyCode);
	bool IsKeyUpOnce(gameplay::Keyboard::Key keyCode);

	void UpdateKeyboardData(gameplay::Keyboard::KeyEvent evt, int key);
	void UpdateMouseData(gameplay::Mouse::MouseEvent evt, int x, int y, int wheelDelta);

	gameplay::Vector2 GetDriveDirection();
	float GetDriveDirectionX();
	float GetDriveDirectionY();
	gameplay::Vector2 GetAimDirection();
	float GetAimDirectionX();
	float GetAimDirectionY();
	bool IsPrimaryShooting();
	bool IsPrimaryShootingOnce();
	float GetPrimaryShootingValue();
	bool IsSecondaryShooting();
	bool IsSecondaryShootingOnce();
	bool IsSecondaryShootingReleased();
	float GetSecondaryShootingValue();

	// TODO(Almar): fill in rest of the menu actions
	bool MenuUp();
	bool MenuUpOnce();
	bool MenuRight();
	bool MenuRightOnce();
	bool MenuDown();
	bool MenuDownOnce();
	bool MenuLeft();
	bool MenuLeftOnce();
	bool MenuSelect();
	bool MenuSelectOnce();
	bool MenuBack();
	bool MenuBackOnce();
	bool MenuPause();
	bool MenuPauseOnce();
	
private:

	void _UpdatePreviousKeyState(gameplay::Keyboard::Key key);

	// keyboard
	std::map<int, bool> _previousKeyStates;
	std::map<int, bool> _keyStates;

	// mnouse
	gameplay::Mouse::MouseEvent _previousMouseEvent;
	gameplay::Mouse::MouseEvent _mouseEvent = gameplay::Mouse::MOUSE_RELEASE_LEFT_BUTTON;
	gameplay::Vector2 _previousMousePosition;
	gameplay::Vector2 _mousePosition;
	int _previousMouseWheelDelta;
	int _mouseWheelDelta;

};