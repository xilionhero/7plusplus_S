#include "PlayerTank.h"
#include "TankBuilder.h"

#include "src/GTGame.h"

//PlayerTank::PlayerTank(TankBuilder tankBuilder) : MyPhysicsObject()
//{
//	
//}


PlayerTank::~PlayerTank()
{
	SAFE_RELEASE(tTurretNode);
	//SAFE_RELEASE(tBaseNode);
	//SAFE_RELEASE(tankNode);
	//SAFE_RELEASE(tankBaseModel);
	//SAFE_RELEASE(tankTurretModel);
	//SAFE_RELEASE(tankMaterial);
}

void PlayerTank::Initialize()
{
	_crosshairDistance = 1.0;
	_timer = 0;
	_cooldown = 2.0f;
	_timer = _cooldown; // First shot is free

	firePartEmit = ParticleEmitter::create("res/particles/fire.particle");
	firePartEmit->setTexture("res/particles/fire.png", firePartEmit->getBlendMode());
	firePartNode = Node::create("firePartNode");
	firePartNode->setDrawable(firePartEmit);
	tTurretNode->addChild(firePartNode);
	firePartNode->setTranslationX(1.5);
	firePartNode->setTranslationY(0.5);

	_hitSound = AudioSource::create("res/common/hit.wav", false);
	_shootSound = AudioSource::create("res/common/shot.wav", false);
	_drivingSound = AudioSource::create("res/common/engine.wav", false);

	//Set the volume of the driving sound and allow looping
	_drivingSound->setGain(0.5f);
	_drivingSound->setLooped(true);

	//The driving sound needs to be activated from the start. However, we pause it immediately, because no tank is driving
	//We can resume the sound once the tank starts driving.
	_drivingSound->play();
	_drivingSound->pause();

	_pCrosshair = _pCrosshairBuilder->BuildCrosshair();
}

void PlayerTank::Reset() 
{
	body->setPosition(spawnPoint);
	tankHP = 100;
	//myNode->setTranslation(spawnPoint);
}

void PlayerTank::Update(float elapsedTime) 
{
	//RotateTurret(elapsedTime);

	firePartEmit->update(elapsedTime * 1000);

	if (_timer < _cooldown)
	{
		_timer += elapsedTime;
	}

	// hot swap between handlers
	if (_pKeyboardMouseHandler->IsKeyDownOnce(Keyboard::KEY_SPACE))
	{
		// if the current handler is a gamepadhandler
		if (dynamic_cast<GamepadHandler*>(_pActiveInputHandler) != nullptr || _pGamepadHandler == NULL)
		{
			_pActiveInputHandler = _pKeyboardMouseHandler;
		}
		else
		{
			_pActiveInputHandler = _pGamepadHandler;
		}
	}

	if (_pActiveInputHandler) {

		if (_pActiveInputHandler->IsPrimaryShooting())
		{
			PrimaryShot();
		}
		
		if (_pActiveInputHandler->IsSecondaryShooting() && CanShoot())
		{
			_crosshairDistance += 0.2;

			// hack
			gameplay::Vector2 crosshairDirection = _pActiveInputHandler->GetAimDirection() * 100;
			crosshairDirection.normalize();

			gameplay::Vector3 offset = gameplay::Vector3(crosshairDirection.x * _crosshairDistance, 0.01, crosshairDirection.y * _crosshairDistance);
			_pCrosshair->myNode->setTranslation(myNode->getTranslation() + offset);
		}
		else
		{
			if (_crosshairDistance > 1)
			{
				_crosshairDistance = 0;

				SecondaryShot();

				_pCrosshair->myNode->setTranslation(gameplay::Vector3(999, 999, 999));
			}
		}

		if (_pActiveInputHandler)
		{
			Vector2 axis;
			Vector2 axis2;

			
			axis = _pActiveInputHandler->GetDriveDirection(); //Axis of the left control stick
			axis2 = _pActiveInputHandler->GetAimDirection(); //Axis of the right control stick
			if(	axis2.length() != 0)
				aimDir = axis2;
			

			float aimAngle = atan2(-axis.y, axis.x); //Angle of the left control stick in radians
			float aimAngleT = atan2(-axis2.y, axis2.x); //Angle of the right control stick in radians

			if ((axis2.x > 0.2f || axis2.x < -0.2f) || (axis2.y > 0.2f || axis2.y < -0.2f))
			{
				tTurretNode->setRotation(gameplay::Vector3(0, 1, 0), aimAngleT);
			}

			if ((axis.x > 0.2f || axis.x < -0.2f) || (axis.y > 0.2f || axis.y < -0.2f))
			{
				//myNode->translate(axis.x * 2.0 * elapsedTime, 0, axis.y * 4.5 * elapsedTime);
				body->setVelocity(cyclone::Vector3(axis.x * 420 * elapsedTime, 0, axis.y * 420 * elapsedTime));
				tBaseNode->setRotation(gameplay::Vector3(0, 1, 0), aimAngle);
				body->setOrientation(tBaseNode->getRotation().w, tBaseNode->getRotation().x, tBaseNode->getRotation().y, tBaseNode->getRotation().z);
				_drivingSound->resume();
			}
			else
			{
				body->setVelocity(cyclone::Vector3(0, 0, 0));
				_drivingSound->pause();
			}

			/*if (padHandler->IsButtonDownOnce(Gamepad::BUTTON_A)) {
				_soundClip->play();
			}
			if (padHandler->IsButtonDownOnce(Gamepad::BUTTON_B)) {
				_soundClip->stop();
			}*/
		}
	}

	body->integrate(elapsedTime);
	tankCollisionBox.calculateInternals();
}

void PlayerTank::RotateTurret()
{
	//tTurretNode->rotateY(MATH_DEG_TO_RAD((float)elapsedTime / 1000.0f * 180.0f));

}

void PlayerTank::SetBulletBuilder(BulletBuilder * bulletBuilder)
{
	_pBulletBuilder = bulletBuilder;
}

void PlayerTank::SetCrosshairBuilder(CrosshairBuilder * crosshairBuilder)
{
	_pCrosshairBuilder = crosshairBuilder;
}

bool PlayerTank::CanShoot()
{
	return _timer >= _cooldown;
}

void PlayerTank::PrimaryShot()
{
	if (CanShoot())
	{
		aimDir = aimDir.normalize();
		//gameplay::Vector2 aimDir = _pActiveInputHandler->GetAimDirection().normalize();
		gameplay::Vector3 spawnPos = myNode->getTranslation() + gameplay::Vector3(aimDir.x * 1.5, tTurretNode->getTranslationY() + 0.3, aimDir.y * 1.5);

		Bullet* pBullet = _pBulletBuilder->BuildBullet();
		pBullet->tankNr = tankNr;
		pBullet->FireFrom(spawnPos, gameplay::Vector3(aimDir.x, 0, aimDir.y), false);

		// Replace with the end of the turret node position and the getAimDirection 
		
		//pBullet->FireFrom(, tTurretNode->getRotation());
		_timer -= _cooldown;
		//
		fxMan->SpawnShockwave(myNode->getTranslation() + gameplay::Vector3(aimDir.x * 1.75, tTurretNode->getTranslationY(), aimDir.y * 1.75), aimDir);
		firePartEmit->emitOnce(50);

		// add to object controller
		objCont->AddToList(pBullet);

		_shootSound->play();
	}
}

void PlayerTank::SecondaryShot()
{
	if (CanShoot())
	{
		gameplay::Vector2 aimDir = _pActiveInputHandler->GetAimDirection().normalize();
		gameplay::Vector3 spawnPos = myNode->getTranslation() + gameplay::Vector3(aimDir.x * 1.5, tTurretNode->getTranslationY() + 0.3, aimDir.y * 1.5);

		gameplay::Vector3 line = _pCrosshair->myNode->getTranslation() - myNode->getTranslation();
		float distance = line.length();

		Bullet* pBullet = _pBulletBuilder->BuildBullet();
		pBullet->tankNr = tankNr;

		// dont ask me, it works.. its not perfect but who will notice
		float magicNumber = (sqrt(distance) * 2) + sin(9.81 / distance);
		pBullet->FireFrom(spawnPos, gameplay::Vector3(line.x / sqrt(powf(distance, 0.5)), magicNumber, line.z / sqrt(powf(distance, 0.5))), true);

		_timer -= _cooldown;

		fxMan->SpawnShockwave(myNode->getTranslation() + gameplay::Vector3(aimDir.x * 1.75, tTurretNode->getTranslationY(), aimDir.y * 1.75), aimDir);
		firePartEmit->emitOnce(50);

		// add to object controller
		objCont->AddToList(pBullet);
	}
}

void PlayerTank::GetHit(PlayerTank* shooter)
{
	_hitSound->play();
	tankHP -= 20;
	if (tankHP <= 0)
	{
		shooter->IncreaseScore();
		Reset();
	}
}

void PlayerTank::IncreaseScore()
{
	tankScore += 1;
	Reset();
}