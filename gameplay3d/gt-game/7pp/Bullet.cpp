#include "Bullet.h"

#include "src/GTGame.h"


Bullet::~Bullet()
{
	if (smokePartNode)
		SAFE_RELEASE(smokePartNode);
}

Bullet::Bullet()
{
	speed = 30;
}

void Bullet::Update(float t)
{
	_lifeTime += t;
	if (_lifeTime > 3.0F && activeInScene)
	{
		myNode->setTranslation(Vector3(0, 999.0F, 0));
		activeInScene = false;
		if (smokePartNode)
			SAFE_RELEASE(smokePartNode);
	}
	else
	{
		smokePartEmit->update(t * 1000);
		body->integrate(t);
		bulletCollisionBox.calculateInternals();

		float angle = atan2(-body->velocity.z, body->velocity.x);
		myNode->setRotation(gameplay::Vector3(0, 1, 0), angle);
	}
}

// Needs a spawn position (end of turret?) and the 2D direction it's aiming to (from the input getAimDirection)
void Bullet::FireFrom(gameplay::Vector3 spawnPos, gameplay::Vector3 direction, bool useGravity)
{
	myNode->setTranslation(spawnPos);
	body->setPosition(spawnPos);


	float hax = speed;
	if (useGravity)
	{
		body->acceleration.y = -9.81;
		hax = 1;
	}
	body->addVelocity(direction * hax);



	//speed= 100;
	//cyclone::Vector3 dir(direction.y, 0, direction.w);
	//dir.normalise();

	//myNode->setRotation(direction);
	//body->setOrientation(cyclone::Quaternion(direction.w, direction.x, direction.y, direction.z));
	//myNode->setTranslation(spawnPos);
	//// Rigidbody needs to move to spawnPos.
	//body->setPosition(cyclone::Vector3(spawnPos.x, spawnPos.y, spawnPos.z));
	//body->setVelocity (myNode->getRightVector() * 100);
	////rotation = (float)atan2(-direction.y, direction.x);
	////myNode->rotateY(rotation);
	////body->addVelocity(cyclone::Vector3(0.2, 0, 0));
	////myNode->setRotation(direction);

	//// Add smoke particle emitter to node.
	smokePartEmit = ParticleEmitter::create("res/particles/smoke.particle");
	smokePartEmit->setTexture("res/particles/smoke.png", smokePartEmit->getBlendMode());
	smokePartNode = Node::create("smokePartNode");
	smokePartNode->setDrawable(smokePartEmit);
	//// Rotation relative to parent.
	////smokePartNode->setRotation(Vector3(0, 0, 1), 3.14);
	smokePartEmit->start();

	myNode->addChild(smokePartNode);
	////myNode->getScene()->addNode(smokePartNode);

}

void Bullet::SetPhysicsValues(float bMass, float bDamping)
{
	mass = bMass;
	damping = bDamping;
	// Create a pointer to the rigidbody for accessing the object from tank.
	body = new cyclone::RigidBody();
	body->myNode = myNode;

	// Instance of the body that must be given to the base class.
	bulletCollisionBox.body = body;

	// Set the half size of the collision box.
	bulletCollisionBox.halfSize = cyclone::Vector3(0.2, 0.2, 0.2);
	body->setPosition(-1000, -1000, -1000);
	//body->setOrientation(1, 0, 0, 0);
	//body->setVelocity(0, 0, 0);
	//body->setRotation(0, 0, 0);

	body->setMass(mass);

	// Set inertia tensor (Mass for rotation)
	cyclone::Matrix3 tensor;
	tensor.setBlockInertiaTensor(bulletCollisionBox.halfSize, mass);
	body->setInertiaTensor(tensor);

	body->setDamping(0.99f, 0.8f);
	body->clearAccumulators();
	//body->setAcceleration(cyclone::Vector3::GRAVITY);

	body->setCanSleep(false);
	body->setAwake(true);

	body->calculateDerivedData();
	bulletCollisionBox.calculateInternals();
}