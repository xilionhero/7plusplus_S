#include "KeyboardMouseHandler.h"

#include "src/GTGame.h"

#include <Windows.h>
#include <WinUser.h>

using namespace gameplay;

void KeyboardMouseHandler::_UpdatePreviousKeyState(gameplay::Keyboard::Key key)
{
	_previousKeyStates[key] = _keyStates[key];
}

void KeyboardMouseHandler::UpdateKeyboardData(Keyboard::KeyEvent evt, int key)
{
	switch (evt)
	{
	case Keyboard::KeyEvent::KEY_PRESS:
		_keyStates[key] = true;
		break;

	case Keyboard::KeyEvent::KEY_RELEASE:
		_keyStates[key] = false;
		break;
	}
}

void KeyboardMouseHandler::UpdateMouseData(Mouse::MouseEvent evt, int x, int y, int wheelDelta)
{
	_previousMouseEvent = _mouseEvent;
	_mouseEvent = evt;

	_previousMousePosition = _mousePosition;
	_mousePosition.x = (float)x;
	_mousePosition.y = (float)y;

	_previousMouseWheelDelta = _mouseWheelDelta;
	_mouseWheelDelta = wheelDelta;
}

bool KeyboardMouseHandler::IsKeyDown(Keyboard::Key keyCode)
{
	return _keyStates[keyCode];
}

bool KeyboardMouseHandler::IsKeyUp(Keyboard::Key keyCode)
{
	return !_keyStates[keyCode];
}

bool KeyboardMouseHandler::IsKeyDownOnce(Keyboard::Key keyCode)
{
	bool result = _keyStates[keyCode] && !_previousKeyStates[keyCode];

	_UpdatePreviousKeyState(keyCode);

	return result;
}

bool KeyboardMouseHandler::IsKeyUpOnce(Keyboard::Key keyCode)
{
	bool result = !_keyStates[keyCode] && _previousKeyStates[keyCode];

	_UpdatePreviousKeyState(keyCode);

	return result;
}

Vector2 KeyboardMouseHandler::GetDriveDirection()
{
	return Vector2(GetDriveDirectionX(), GetDriveDirectionY());
}

float KeyboardMouseHandler::GetDriveDirectionX()
{
	float directionX = 0.0;

	if (IsKeyDown(Keyboard::KEY_A))
	{
		directionX = -1.0;
	}
	else if (IsKeyDown(Keyboard::KEY_D))
	{
		directionX = 1.0;
	}

	return directionX;
}

float KeyboardMouseHandler::GetDriveDirectionY()
{
	float directionY = 0.0;

	if (IsKeyDown(Keyboard::KEY_W))
	{
		directionY = -1.0;
	}
	else if (IsKeyDown(Keyboard::KEY_S))
	{
		directionY = 1.0;
	}

	return directionY;
}

Vector2 KeyboardMouseHandler::GetAimDirection()
{
	return Vector2(GetAimDirectionX(), GetAimDirectionY());
}

float KeyboardMouseHandler::GetAimDirectionX()
{
	float directionX = 0.0;

	if (IsKeyDown(Keyboard::KEY_LEFT_ARROW))
	{
		directionX = -1.0;
	}
	else if (IsKeyDown(Keyboard::KEY_RIGHT_ARROW))
	{
		directionX = 1.0;
	}

	return directionX;
}

float KeyboardMouseHandler::GetAimDirectionY()
{
	float directionY = 0.0;

	if (IsKeyDown(Keyboard::KEY_UP_ARROW))
	{
		directionY = -1.0;
	}
	else if (IsKeyDown(Keyboard::KEY_DOWN_ARROW))
	{
		directionY = 1.0;
	}

	return directionY;
}

bool KeyboardMouseHandler::IsPrimaryShooting()
{
	return _mouseEvent == Mouse::MOUSE_PRESS_LEFT_BUTTON;
}

bool KeyboardMouseHandler::IsPrimaryShootingOnce()
{
	return _mouseEvent == Mouse::MOUSE_PRESS_LEFT_BUTTON;
}

float KeyboardMouseHandler::GetPrimaryShootingValue()
{
	return (_mouseEvent == Mouse::MOUSE_PRESS_LEFT_BUTTON) ? 1.0f : 0.0f;
}

bool KeyboardMouseHandler::IsSecondaryShooting()
{
	return _mouseEvent == Mouse::MOUSE_PRESS_RIGHT_BUTTON;
}

bool KeyboardMouseHandler::IsSecondaryShootingOnce()
{
	return _mouseEvent == Mouse::MOUSE_PRESS_RIGHT_BUTTON;
}

bool KeyboardMouseHandler::IsSecondaryShootingReleased()
{
	return false;
}

float KeyboardMouseHandler::GetSecondaryShootingValue()
{
	return (_mouseEvent == Mouse::MOUSE_PRESS_RIGHT_BUTTON) ? 1.0f : 0.0f;
}

bool KeyboardMouseHandler::MenuUp()
{
	return false;
}

bool KeyboardMouseHandler::MenuUpOnce()
{
	return false;
}

bool KeyboardMouseHandler::MenuRight()
{
	return false;
}

bool KeyboardMouseHandler::MenuRightOnce()
{
	return false;
}

bool KeyboardMouseHandler::MenuDown()
{
	return false;
}

bool KeyboardMouseHandler::MenuDownOnce()
{
	return false;
}

bool KeyboardMouseHandler::MenuLeft()
{
	return false;
}

bool KeyboardMouseHandler::MenuLeftOnce()
{
	return false;
}

bool KeyboardMouseHandler::MenuSelect()
{
	return false;
}

bool KeyboardMouseHandler::MenuSelectOnce()
{
	return false;
}

bool KeyboardMouseHandler::MenuBack()
{
	return false;
}

bool KeyboardMouseHandler::MenuBackOnce()
{
	return false;
}

bool KeyboardMouseHandler::MenuPause()
{
	return false;
}

bool KeyboardMouseHandler::MenuPauseOnce()
{
	return false;
}