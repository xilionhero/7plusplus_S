#pragma once
#include "Scene.h"
#include "Crosshair.h"

class CrosshairBuilder
{
public:

	CrosshairBuilder(gameplay::Scene* scene);

	~CrosshairBuilder();
	void Initialize();

	//Method that will create the Crosshair in the scene
	Crosshair* BuildCrosshair();

	gameplay::Scene* pScene;
	gameplay::Node* pOriginalNode;
	gameplay::Model* pCrosshairModel;
	gameplay::Material* pCrosshairMaterial;
};

