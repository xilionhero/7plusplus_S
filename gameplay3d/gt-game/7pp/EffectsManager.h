#pragma once
#include "Scene.h"
#include "Node.h"
#include "Shockwave.h"

class GTGame;

/** Class that contains all effects that are spawned in game.
*
* This class holds all the effects that are spawned from somewhere in the game.
* Effects include all particle effects and the shockwave for now.
* This manager was created because particle systems and _pShockwaves have to be individually and manually updated and rendered.
*/
class EffectsManager
{
public:
	EffectsManager(gameplay::Scene* activeScene, GTGame* mainScr);
	~EffectsManager();

	/*
	* Update all the spawned effects every frame.
	*/
	void Update(float totalAbsTime);

	void SpawnShockwave(gameplay::Vector3 spawnPos, gameplay::Vector2 direction);

	// Method to clear the shockwaveslist whenever a new round starts.
	void ClearShockwavesList(gameplay::Scene* pScene);

private:
	std::vector<Shockwave*> _pShockwaves;
	gameplay::Scene* _pActiveScene;
	gameplay::Bundle* _pFxBundle;
	gameplay::Node* _pShockWaveTest;
	gameplay::Node* _pSmokePartNode;
	gameplay::ParticleEmitter* _pSmokePartEmit;
	gameplay::Node* _pFirePartNode;
	gameplay::ParticleEmitter* _pFirePartEmit;
	float _startTime;
	GTGame* _pMainScrR;
};

