#pragma once
#include "ForceRegistry.h"

ForceRegistry::ForceRegistry()
{
}


ForceRegistry::~ForceRegistry()
{
}

void ForceRegistry::add(Rigidbody * myObject, ForceGenerator * forceGenerator)
{
	ForceRegistry registration;
	registration.myPhysicsObject = myObject;
	registration.fg = fg;
	registrations.push_back(registration);
}

void ForceRegistry::updateForces(float duration)
{
	std::vector<ForceRegistry>::iterator i = registrations.begin();

	for (i ; i != registrations.end(); i++)
	{
		i->fg->updateForce(i->myPhysicsObject, duration);
	}

}
